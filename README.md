<div align="center">

# MyERP
### ERP sur-mesure pour Dummy...

---

</div>

<table>
<tr>
<td>
<div>

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=com.dummy.myerp%3Amyerp)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

</div>
</td>
<td>
<div>

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=coverage)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

</div>
</td>
<td>
<div>

[![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=security_rating)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=sqale_rating)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

</div>
</td>
<td>
<div>

[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=bugs)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=com.dummy.myerp%3Amyerp&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=com.dummy.myerp%3Amyerp)

</div>
</td>
<tr>
</table>

----


## Organisation du répertoire

*   `doc` : documentation
*   `docker` : répertoire relatifs aux conteneurs _docker_ utiles pour le projet
    *   `dev` : environnement de développement
*   `src` : code source de l'application

## Variables d'environnement

Ces variables d'environnement sont à mettre en place afin que le projet fonctionne :

* `MYERP_DB_HOST` : Représente l'url pour accèder à la base de données (ex. `localhost` ou `127.0.0.1`).
* `SONAR_TOKEN` : Le token privé du compte sur SonarCloud pour pouvoir scanner le code source.

## Environnement de développement

Les composants nécessaires lors du développement sont disponibles via des conteneurs _docker_.
L'environnement de développement est assemblé grâce à _docker-compose_
(cf docker/dev/docker-compose.yml).

Il comporte :

*   une base de données _PostgreSQL_ contenant un jeu de données de démo (`postgresql://127.0.0.1:9032/db_myerp`)



### Lancement

    cd docker/dev
    docker-compose up


### Arrêt

    cd docker/dev
    docker-compose stop


### Remise à zero

    cd docker/dev
    docker-compose stop
    docker-compose rm -v
    docker-compose up
