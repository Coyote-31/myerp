package com.dummy.myerp.model.bean.comptabilite;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class JournalComptableTest {

    private String vCode;
    private String vLibelle;
    private JournalComptable vJournalComptable;
    private JournalComptable vJournalComptableCopy;
    private String vCodeDiff;
    private String vLibelleDiff;
    private JournalComptable vJournalComptableDiff;

    @BeforeEach
    void init() {
        vCode = "AC";
        vLibelle = "Achat";
        vJournalComptable = new JournalComptable(vCode, vLibelle);
        vJournalComptableCopy = new JournalComptable(vCode, vLibelle);
        vCodeDiff = "VE";
        vLibelleDiff = "Vente";
        vJournalComptableDiff = new JournalComptable(vCodeDiff, vLibelleDiff);
    }

    @AfterEach
    void teardown() {
        vJournalComptable = null;
        vJournalComptableCopy = null;
        vJournalComptableDiff = null;
    }

    @Test
    void hashCode_WhenTwoJournalComptableAreEquals_ReturnSameHashCode() {
        Integer vHash1 = vJournalComptable.hashCode();
        Integer vHash2 = vJournalComptableCopy.hashCode();
        assertThat(vHash1).isEqualTo(vHash2);
    }

    @Test
    void hashCode_WhenTwoJournalComptableAreDifferents_ReturnDifferentHashCode() {
        Integer vHash1 = vJournalComptable.hashCode();
        Integer vHash2 = vJournalComptableDiff.hashCode();
        assertThat(vHash1).isNotEqualTo(vHash2);
    }

    @Test
    void equals_WhenTwoJournalComptableAreEquals_ReturnTrue() {
        assertThat(vJournalComptable.equals(vJournalComptableCopy)).isTrue();
    }

    @Test
    void equals_WhenTwoCompteComptableAreDifferents_ReturnFalse() {
        assertThat(vJournalComptable.equals(vJournalComptableDiff)).isFalse();
    }

    @Test
    void equals_DeepTest() {
        // obj == null
        vJournalComptableDiff = null;
        assertThat(vJournalComptable.equals(vJournalComptableDiff)).isFalse();

        // getClass() != obj.getClass()
        Object vJournalComptableDiffObject = new Object();
        assertThat(vJournalComptable.equals(vJournalComptableDiffObject)).isFalse();

        // code == null && other.code != null
        vJournalComptable.setCode(null);
        vJournalComptableDiff = new JournalComptable();
        vJournalComptableDiff.setCode("JC");
        assertThat(vJournalComptable.equals(vJournalComptableDiff)).isFalse();

        // libelle == null && other.libelle != null
        vJournalComptable.setCode("JC");
        vJournalComptable.setLibelle(null);
        vJournalComptableDiff.setLibelle("Libellé");
        assertThat(vJournalComptable.equals(vJournalComptableDiff)).isFalse();

        // !libelle.equals(other.libelle)
        vJournalComptable.setLibelle("Autre libellé");
        assertThat(vJournalComptable.equals(vJournalComptableDiff)).isFalse();
    }

    @Test
    void toString_returnCorrectString() {
        String vAssertString = "JournalComptable{code='AC', libelle='Achat'}";
        String vJournalComptableString = vJournalComptable.toString();
        assertThat(vJournalComptableString).isEqualTo(vAssertString);
    }

    @Test
    void getByCode_ReturnCorrectJournalComptable() {
        // Create the list of JournalComptable :
        List<JournalComptable> vJournalComptableList = new ArrayList<>();
        vJournalComptableList.add(vJournalComptable);
        vJournalComptableList.add(vJournalComptableDiff);

        assertThat(JournalComptable.getByCode(vJournalComptableList, vCode)).isEqualTo(vJournalComptable);
    }

}
