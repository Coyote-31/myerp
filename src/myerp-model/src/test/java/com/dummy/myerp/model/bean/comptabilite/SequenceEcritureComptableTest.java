package com.dummy.myerp.model.bean.comptabilite;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SequenceEcritureComptableTest {

    @Mock
    private JournalComptable vJournalComptable;

    @Mock
    private JournalComptable vJournalComptableDiff;

    private Integer vAnnee;
    private Integer vDerniereValeur;
    private SequenceEcritureComptable vSequenceEcritureComptable;
    private SequenceEcritureComptable vSequenceEcritureComptableCopy;
    private Integer vAnneeDiff;
    private Integer vDerniereValeurDiff;
    private SequenceEcritureComptable vSequenceEcritureComptableDiff;

    @BeforeEach
    void init() {
        vAnnee = 2016;
        vDerniereValeur = 40;
        vSequenceEcritureComptable = new SequenceEcritureComptable(
                vJournalComptable, vAnnee, vDerniereValeur);
        vSequenceEcritureComptableCopy = new SequenceEcritureComptable(
                vJournalComptable, vAnnee, vDerniereValeur);
        vAnneeDiff = 2017;
        vDerniereValeur = 41;
        vSequenceEcritureComptableDiff = new SequenceEcritureComptable(
                vJournalComptableDiff, vAnneeDiff, vDerniereValeurDiff);
    }

    @AfterEach
    void teardown() {
        vSequenceEcritureComptable = null;
        vSequenceEcritureComptableCopy = null;
        vSequenceEcritureComptableDiff = null;
    }

    @Test
    void hashCode_WhenTwoSequenceEcritureComptableAreEquals_ReturnSameHashCode() {
        Integer vHash1 = vSequenceEcritureComptable.hashCode();
        Integer vHash2 = vSequenceEcritureComptableCopy.hashCode();
        assertThat(vHash1).isEqualTo(vHash2);
    }

    @Test
    void hashCode_WhenTwoSequenceEcritureComptableAreDifferents_ReturnDifferentHashCode() {
        Integer vHash1 = vSequenceEcritureComptable.hashCode();
        Integer vHash2 = vSequenceEcritureComptableDiff.hashCode();
        assertThat(vHash1).isNotEqualTo(vHash2);
    }

    @Test
    void equals_WhenTwoSequenceEcritureComptableAreEquals_ReturnTrue() {
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableCopy)).isTrue();
    }

    @Test
    void equals_WhenTwoSequenceEcritureComptableAreDifferents_ReturnFalse() {
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();
    }

    @Test
    void equals_DeepTest() {
        // this == obj
        vSequenceEcritureComptableDiff = vSequenceEcritureComptable;
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isTrue();

        // obj == null
        vSequenceEcritureComptableDiff = null;
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();

        // getClass() != obj.getClass()
        Object vSequenceEcritureComptableDiffObject = new Object();
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiffObject)).isFalse();

        // journal == null && other.journal != null
        vSequenceEcritureComptable.setJournal(null);
        vSequenceEcritureComptableDiff = new SequenceEcritureComptable();
        vSequenceEcritureComptableDiff.setJournal(new JournalComptable());
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();

        // annee == null && other.annee != null
        vSequenceEcritureComptable.setJournal(new JournalComptable());
        vSequenceEcritureComptable.setAnnee(null);
        vSequenceEcritureComptableDiff.setAnnee(2023);
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();

        // !annee.equals(other.annee)
        vSequenceEcritureComptable.setAnnee(2022);
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();

        // derniereValeur == null && other.derniereValeur != null
        vSequenceEcritureComptable.setAnnee(2023);
        vSequenceEcritureComptable.setDerniereValeur(null);
        vSequenceEcritureComptableDiff.setDerniereValeur(42);
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();

        // !derniereValeur.equals(other.derniereValeur)
        vSequenceEcritureComptable.setDerniereValeur(69);
        assertThat(vSequenceEcritureComptable.equals(vSequenceEcritureComptableDiff)).isFalse();
    }

    @Test
    void toString_returnCorrectString() {

        when(vJournalComptable.getCode()).thenReturn("Mocked_Code");
        String vAssertString =
            "SequenceEcritureComptable{journalCode=Mocked_Code, annee=2016, derniereValeur=40}";
        String vSequenceEcritureComptableString = vSequenceEcritureComptable.toString();
        assertThat(vSequenceEcritureComptableString).isEqualTo(vAssertString);
    }
}
