package com.dummy.myerp.model.bean.comptabilite;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class LigneEcritureComptableTest {

    @Mock
    private CompteComptable vCompteComptable;

    @Mock
    private CompteComptable vCompteComptableDiff;

    private String vLibelle;
    private BigDecimal vDebit;
    private BigDecimal vCredit;
    private LigneEcritureComptable vLigneEcritureComptable;
    private LigneEcritureComptable vLigneEcritureComptableCopy;
    private String vLibelleDiff;
    private BigDecimal vDebitDiff;
    private BigDecimal vCreditDiff;
    private LigneEcritureComptable vLigneEcritureComptableDiff;

    @BeforeEach
    void init() {
        // Compte Comptable

        // Ligne Ecriture Comptable
        vLibelle = "Cartouches d’imprimante";
        vDebit = new BigDecimal("43.95");
        vCredit = null;
        vLigneEcritureComptable = new LigneEcritureComptable(vCompteComptable, vLibelle, vDebit, vCredit);
        vLigneEcritureComptableCopy = new LigneEcritureComptable(vCompteComptable, vLibelle, vDebit, vCredit);
        vLibelleDiff = "TVA 20%";
        vDebitDiff = null;
        vCreditDiff = new BigDecimal("43");
        vLigneEcritureComptableDiff = new LigneEcritureComptable(
                vCompteComptableDiff, vLibelleDiff, vDebitDiff, vCreditDiff);

    }

    @AfterEach
    void teardown() {
        vLigneEcritureComptable = null;
        vLigneEcritureComptableCopy = null;
        vLigneEcritureComptableDiff = null;
    }

    @Test
    void hashCode_WhenTwoLigneEcritureComptableAreEquals_ReturnSameHashCode() {
        Integer vHash1 = vLigneEcritureComptable.hashCode();
        Integer vHash2 = vLigneEcritureComptableCopy.hashCode();
        assertThat(vHash1).isEqualTo(vHash2);
    }

    @Test
    void hashCode_WhenTwoLigneEcritureComptableAreDifferents_ReturnDifferentHashCode() {
        Integer vHash1 = vLigneEcritureComptable.hashCode();
        Integer vHash2 = vLigneEcritureComptableDiff.hashCode();
        assertThat(vHash1).isNotEqualTo(vHash2);
    }

    @Test
    void equals_WhenTwoLigneEcritureComptableAreEquals_ReturnTrue() {
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableCopy)).isTrue();
    }

    @Test
    void equals_WhenTwoLigneEcritureComptableAreDifferents_ReturnFalse() {
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();
    }

    @Test
    void equals_DeepTest() {
        // this == obj
        vLigneEcritureComptableDiff = vLigneEcritureComptable;
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isTrue();

        // obj == null
        vLigneEcritureComptableDiff = null;
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // getClass() != obj.getClass()
        Object vLigneEcritureComptableDiffObject = new Object();
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiffObject)).isFalse();

        // compteComptable == null && other.compteComptable != null
        vLigneEcritureComptable.setCompteComptable(null);
        vLigneEcritureComptableDiff = new LigneEcritureComptable();
        vLigneEcritureComptableDiff.setCompteComptable(new CompteComptable(42, "Libellé"));
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // libelle == null && other.libelle != null
        vLigneEcritureComptable.setCompteComptable(new CompteComptable(42, "Libellé"));
        vLigneEcritureComptable.setLibelle(null);
        vLigneEcritureComptableDiff.setLibelle("Libellé");
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // !libelle.equals(other.libelle)
        vLigneEcritureComptable.setLibelle("Autre Libellé");
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // debit == null && other.debit != null
        vLigneEcritureComptable.setLibelle("Libellé");
        vLigneEcritureComptable.setDebit(null);
        vLigneEcritureComptableDiff.setDebit(new BigDecimal("42"));
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // !debit.equals(other.debit)
        vLigneEcritureComptable.setDebit(new BigDecimal("69"));
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // credit == null && other.credit != null
        vLigneEcritureComptable.setDebit(new BigDecimal("42"));
        vLigneEcritureComptable.setCredit(null);
        vLigneEcritureComptableDiff.setCredit(new BigDecimal("42"));
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();

        // !credit.equals(other.credit)
        vLigneEcritureComptable.setCredit(new BigDecimal("69"));
        assertThat(vLigneEcritureComptable.equals(vLigneEcritureComptableDiff)).isFalse();
    }

    @Test
    void toString_returnCorrectString() {

        when(vCompteComptable.toString()).thenReturn("Mocked_ToString");
        String vAssertString =
        "LigneEcritureComptable{Mocked_ToString, "
            + "libelle='Cartouches d’imprimante', debit=43.95, credit=null}";
        String vLigneEcritureComptableString = vLigneEcritureComptable.toString();
        assertThat(vLigneEcritureComptableString).isEqualTo(vAssertString);
    }

}
