package com.dummy.myerp.model.bean.comptabilite;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

import org.apache.commons.lang3.ObjectUtils;
import org.junit.jupiter.api.Test;

public class EcritureComptableTest {

    private LigneEcritureComptable createLigne(Integer pCompteComptableNumero, String pDebit, String pCredit) {
        BigDecimal vDebit = pDebit == null ? null : new BigDecimal(pDebit);
        BigDecimal vCredit = pCredit == null ? null : new BigDecimal(pCredit);
        String vLibelle = ObjectUtils.defaultIfNull(vDebit, BigDecimal.ZERO)
                .subtract(ObjectUtils.defaultIfNull(vCredit, BigDecimal.ZERO)).toPlainString();
        LigneEcritureComptable vRetour = new LigneEcritureComptable(
                new CompteComptable(pCompteComptableNumero),
                vLibelle,
                vDebit,
                vCredit);
        return vRetour;
    }

    // ===== getTotalDebit ===== //

    @Test
    public void getTotalDebit_returnsDebitSum_ofScaleZeroBigDecimals() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Total Débit de BigDecimals à scale zero");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "40", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "10", null));
        assertEquals(BigDecimal.valueOf(100), vEcriture.getTotalDebit(), vEcriture.toString());
    }

    @Test
    public void getTotalDebit_returnsDebitSum_ofScaleTwoBigDecimals() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Total Débit de BigDecimals à scale deux");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "49.50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "40.50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "10", null));
        assertEquals(BigDecimal.valueOf(10000, 2), vEcriture.getTotalDebit(), vEcriture.toString());
    }

    // ===== getTotalCredit ===== //

    @Test
    public void getTotalCredit_returnsCreditSum_ofScaleZeroBigDecimals() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Total Crédit de BigDecimals à scale zero");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, null, "50"));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, null, "40"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "10"));
        assertEquals(BigDecimal.valueOf(100), vEcriture.getTotalCredit(), vEcriture.toString());
    }

    @Test
    public void getTotalCredit_returnsCreditSum_ofScaleTwoBigDecimals() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Total Crédit de BigDecimals à scale deux");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, null, "49.50"));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, null, "40.50"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, null));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "10"));
        assertEquals(BigDecimal.valueOf(10000, 2), vEcriture.getTotalCredit(), vEcriture.toString());
    }

    // ===== isEquilibree ===== //

    @Test
    public void isEquilibree() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("Equilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "200.50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "100.50", "33"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "301"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "40", "7"));
        assertTrue(vEcriture.isEquilibree(), vEcriture.toString());

        vEcriture.getListLigneEcriture().clear();
        vEcriture.setLibelle("Non équilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "10", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "20", "1"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "30"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "1", "2"));
        assertFalse(vEcriture.isEquilibree(), vEcriture.toString());
    }

    // ===== Vérifie le respect de la RG_Compta_4 ===== //
    /* Les montants des lignes d'écriture sont signés et peuvent
       prendre des valeurs négatives (même si cela est peu fréquent). */

    @Test
    public void RG_Compta_4() {
        EcritureComptable vEcriture;
        vEcriture = new EcritureComptable();

        vEcriture.setLibelle("RG_4 Equilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "-200.50", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "-100.50", "-33"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "-301"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "-40", "-7"));
        assertTrue(vEcriture.isEquilibree(), vEcriture.toString());

        vEcriture.getListLigneEcriture().clear();
        vEcriture.setLibelle("RG_4 Non équilibrée");
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "-10", null));
        vEcriture.getListLigneEcriture().add(this.createLigne(1, "-20", "-1"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, null, "-30"));
        vEcriture.getListLigneEcriture().add(this.createLigne(2, "-1", "-2"));
        assertFalse(vEcriture.isEquilibree(), vEcriture.toString());
    }

    @Test
    public void hashCode_WhenTwoEcritureComptableAreEquals_ReturnSameHashCode() throws Exception {

        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));
        Integer vHash1 = vEcritureComptable.hashCode();

        EcritureComptable vEcritureComptableCopy;
        vEcritureComptableCopy = new EcritureComptable();
        vEcritureComptableCopy.setId(-1);
        vEcritureComptableCopy.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatterCopy = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptableCopy.setDate(dateFormatterCopy.parse("1-Janvier-2023"));
        vEcritureComptableCopy.setLibelle("Cartouches d’imprimante");
        vEcritureComptableCopy.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptableCopy.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));
        Integer vHash2 = vEcritureComptableCopy.hashCode();

        assertEquals(vHash1, vHash2);
    }

    @Test
    public void hashCode_WhenTwoEcritureComptableAreDifferents_ReturnDifferentHashCode() throws Exception {

        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));
        Integer vHash1 = vEcritureComptable.hashCode();

        EcritureComptable vEcritureComptableDiff;
        vEcritureComptableDiff = new EcritureComptable();
        vEcritureComptableDiff.setId(-2);
        vEcritureComptableDiff.setReference("AC-2016/00002");
        SimpleDateFormat dateFormatterCopy = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptableDiff.setDate(dateFormatterCopy.parse("2-Janvier-2023"));
        vEcritureComptableDiff.setLibelle("Cartouches");
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(1, "100", null));
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(2, null, "100"));
        Integer vHash2 = vEcritureComptableDiff.hashCode();

        assertNotEquals(vHash1, vHash2);
    }

    @Test
    public void equals_WhenTwoEcritureComptableAreEquals_ReturnTrue() throws Exception {

        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));

        EcritureComptable vEcritureComptableCopy;
        vEcritureComptableCopy = new EcritureComptable();
        vEcritureComptableCopy.setId(-1);
        vEcritureComptableCopy.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatterCopy = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptableCopy.setDate(dateFormatterCopy.parse("1-Janvier-2023"));
        vEcritureComptableCopy.setLibelle("Cartouches d’imprimante");
        vEcritureComptableCopy.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptableCopy.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));

        assertEquals(vEcritureComptable, vEcritureComptableCopy);
    }

    @Test
    public void equals_WhenTwoEcritureComptableAreDifferents_ReturnFalse() throws Exception {

        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));

        EcritureComptable vEcritureComptableDiff;
        vEcritureComptableDiff = new EcritureComptable();
        vEcritureComptableDiff.setId(-2);
        vEcritureComptableDiff.setReference("AC-2016/00002");
        SimpleDateFormat dateFormatterCopy = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptableDiff.setDate(dateFormatterCopy.parse("2-Janvier-2023"));
        vEcritureComptableDiff.setLibelle("Cartouches");
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(1, "100", null));
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(2, null, "100"));

        assertNotEquals(vEcritureComptable, vEcritureComptableDiff);
    }

    @Test
    public void equals_DeepTesting() throws Exception {

        // Prepare
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));

        // this == obj
        EcritureComptable vEcritureComptableDiff = vEcritureComptable;
        assertEquals(vEcritureComptableDiff, vEcritureComptable);

        // obj == null
        vEcritureComptableDiff = null;
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // getClass() != obj.getClass()
        Object vEcritureComptableDiffObject = new Object();
        assertNotEquals(vEcritureComptableDiffObject, vEcritureComptable);

        // id == null && other.id != null
        vEcritureComptable.setId(null);
        vEcritureComptableDiff = new EcritureComptable();
        vEcritureComptableDiff.setId(-1);
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // journal == null && other.journal != null
        vEcritureComptable.setId(-1);
        vEcritureComptable.setJournal(null);
        vEcritureComptableDiff.setJournal(new JournalComptable());
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // !journal.equals(other.journal)
        vEcritureComptable.setJournal(new JournalComptable("JN", "Journal"));
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // reference == null && other.reference != null
        vEcritureComptable.setJournal(new JournalComptable());
        vEcritureComptable.setReference(null);
        vEcritureComptableDiff.setReference("JN-2023/00001");
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // !reference.equals(other.reference)
        vEcritureComptable.setReference("JN-2023/00002");
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // date == null && other.date != null
        vEcritureComptable.setReference("JN-2023/00001");
        vEcritureComptable.setDate(null);
        vEcritureComptableDiff.setDate(dateFormatter.parse("1-Janvier-2023"));
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // !date.equals(other.date)
        vEcritureComptable.setDate(dateFormatter.parse("2-Janvier-2023"));
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // libelle == null && other.libelle != null
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle(null);
        vEcritureComptableDiff.setLibelle("Cartouches d’imprimante");
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // !libelle.equals(other.libelle)
        vEcritureComptable.setLibelle("Autre libellé");
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);

        // !listLigneEcriture.equals(other.listLigneEcriture)
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(1, "50", null));
        vEcritureComptableDiff.getListLigneEcriture().add(this.createLigne(2, null, "50"));
        assertNotEquals(vEcritureComptableDiff, vEcritureComptable);
    }

    @Test
    public void toString_returnCorrectString() throws Exception {

        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();

        vEcritureComptable.setId(-1);
        vEcritureComptable.setReference("AC-2016/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("Europe/Paris"));
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Cartouches d’imprimante");
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(1, "100.50", null));
        vEcritureComptable.getListLigneEcriture().add(this.createLigne(2, null, "100.50"));

        String vAssertString = "EcritureComptable{id=-1, journal=null, reference='AC-2016/00001', date=Sun Jan 01 00:00:00 CET 2023, "
                + "libelle='Cartouches d’imprimante', totalDebit=100.50, totalCredit=100.50, listLigneEcriture=[\n"
                + "LigneEcritureComptable{CompteComptable{numero=1, libelle='null'}, "
                + "libelle='100.50', debit=100.50, credit=null}\n"
                + "LigneEcritureComptable{CompteComptable{numero=2, libelle='null'}, "
                + "libelle='-100.50', debit=null, credit=100.50}\n"
                + "]}";
        String vEcritureComptableString = vEcritureComptable.toString();
        assertEquals(vAssertString, vEcritureComptableString);
    }

}
