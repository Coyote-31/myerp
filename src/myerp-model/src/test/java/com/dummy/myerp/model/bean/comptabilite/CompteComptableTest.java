package com.dummy.myerp.model.bean.comptabilite;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompteComptableTest {

    private int vNumero;
    private String vLibelle;
    private CompteComptable vCompteComptable;
    private CompteComptable vCompteComptableCopy;
    private int vNumeroDiff;
    private String vLibelleDiff;
    private CompteComptable vCompteComptableDiff;

    @BeforeEach
    void init() {
        vNumero = 401;
        vLibelle = "Fournisseurs";
        vCompteComptable = new CompteComptable(vNumero, vLibelle);
        vCompteComptableCopy = new CompteComptable(vNumero, vLibelle);
        vNumeroDiff = 411;
        vLibelleDiff = "Clients";
        vCompteComptableDiff = new CompteComptable(vNumeroDiff, vLibelleDiff);
    }

    @AfterEach
    void teardown() {
        vCompteComptable = null;
        vCompteComptableCopy = null;
        vCompteComptableDiff = null;
    }

    @Test
    void hashCode_WhenTwoCompteComptableAreEquals_ReturnSameHashCode() {
        Integer vHash1 = vCompteComptable.hashCode();
        Integer vHash2 = vCompteComptableCopy.hashCode();
        assertThat(vHash1).isEqualTo(vHash2);
    }

    @Test
    void hashCode_WhenTwoCompteComptableAreDifferents_ReturnDifferentHashCode() {
        Integer vHash1 = vCompteComptable.hashCode();
        Integer vHash2 = vCompteComptableDiff.hashCode();
        assertThat(vHash1).isNotEqualTo(vHash2);
    }

    @Test
    void equals_WhenTwoCompteComptableAreEquals_ReturnTrue() {
        assertThat(vCompteComptable.equals(vCompteComptableCopy)).isTrue();
    }

    @Test
    void equals_WhenTwoCompteComptableAreDifferents_ReturnFalse() {
        assertThat(vCompteComptable.equals(vCompteComptableDiff)).isFalse();
    }

    @Test
    void equals_WhenTwoCompteComptableAreDifferents_ReturnFalse_Deep() {
        // Null
        vCompteComptableDiff = null;
        assertThat(vCompteComptable.equals(vCompteComptableDiff)).isFalse();
        // getClass() != obj.getClass()
        Object vCompteComptableDiffObject = new Object();
        assertThat(vCompteComptable.equals(vCompteComptableDiffObject)).isFalse();
        // numero == null && other.numero != null
        vCompteComptable.setNumero(null);
        vCompteComptableDiff = new CompteComptable();
        vCompteComptableDiff.setNumero(401);
        vCompteComptableDiff.setLibelle("Fournisseurs");
        assertThat(vCompteComptable.equals(vCompteComptableDiff)).isFalse();
        // libelle == null && other.libelle != null
        vCompteComptable.setNumero(401);
        vCompteComptable.setLibelle(null);
        assertThat(vCompteComptable.equals(vCompteComptableDiff)).isFalse();
        // !libelle.equals(other.libelle)
        vCompteComptable.setLibelle("Autre");
        assertThat(vCompteComptable.equals(vCompteComptableDiff)).isFalse();
    }

    @Test
    void toString_returnCorrectString() {
        String vAssertString = "CompteComptable{numero=401, libelle='Fournisseurs'}";
        String vCompteComptableString = vCompteComptable.toString();
        assertThat(vCompteComptableString).isEqualTo(vAssertString);
    }

    @Test
    void getByNumero_ReturnCorrectCompteComptable() {
        // Create the list of CompteComptable :
        List<CompteComptable> vCompteComptableList = new ArrayList<>();
        vCompteComptableList.add(vCompteComptable);
        vCompteComptableList.add(vCompteComptableDiff);

        assertThat(CompteComptable.getByNumero(vCompteComptableList, vNumero)).isEqualTo(vCompteComptable);
    }
}
