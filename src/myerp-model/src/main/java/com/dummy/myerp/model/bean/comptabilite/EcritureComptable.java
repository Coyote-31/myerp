package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import org.apache.commons.lang3.StringUtils;

/**
 * Bean représentant une Écriture Comptable
 */
public class EcritureComptable {

    // ==================== Attributs ====================
    /** The Id. */
    private Integer id;
    /** Journal comptable */
    @NotNull
    private JournalComptable journal;
    /** The Reference. */
    @Pattern(regexp = "[A-Z]{2}-\\d{4}/\\d{5}")
    private String reference;
    /** The Date. */
    @NotNull
    private Date date;

    /** The Libelle. */
    @NotNull
    @Size(min = 1, max = 200)
    private String libelle;

    /** La liste des lignes d'écriture comptable. */
    @Valid
    @Size(min = 2, message = "L'écriture comptable doit avoir au moins deux lignes : "
            + "une ligne au débit et une ligne au crédit.")
    private final List<LigneEcritureComptable> listLigneEcriture = new ArrayList<>();

    /**
     * Instancie une nouvelle {@link EcritureComptable}
     */
    public EcritureComptable() {
        // Classe utilitaire :
        // Les attributs sont initialisés et modifiés par les accesseurs
    }

    // ==================== Getters/Setters ====================
    public Integer getId() {
        return id;
    }

    public void setId(Integer pId) {
        id = pId;
    }

    public JournalComptable getJournal() {
        return journal;
    }

    public void setJournal(JournalComptable pJournal) {
        journal = pJournal;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String pReference) {
        reference = pReference;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date pDate) {
        date = pDate;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }

    public List<LigneEcritureComptable> getListLigneEcriture() {
        return listLigneEcriture;
    }

    /**
     * Calcul et renvoie le total des montants au débit des lignes d'écriture
     *
     * @return {@link BigDecimal}, {@link BigDecimal#ZERO} si aucun montant au débit
     */
    // TODONE à tester
    public BigDecimal getTotalDebit() {
        BigDecimal vRetour = BigDecimal.ZERO;
        for (LigneEcritureComptable vLigneEcritureComptable : listLigneEcriture) {
            if (vLigneEcritureComptable.getDebit() != null) {
                vRetour = vRetour.add(vLigneEcritureComptable.getDebit());
            }
        }
        return vRetour;
    }

    /**
     * Calcul et renvoie le total des montants au crédit des lignes d'écriture
     *
     * @return {@link BigDecimal}, {@link BigDecimal#ZERO} si aucun montant au
     *         crédit
     */
    public BigDecimal getTotalCredit() {
        BigDecimal vRetour = BigDecimal.ZERO;
        for (LigneEcritureComptable vLigneEcritureComptable : listLigneEcriture) {
            if (vLigneEcritureComptable.getCredit() != null) {
                vRetour = vRetour.add(vLigneEcritureComptable.getCredit());
            }
        }
        return vRetour;
    }

    /**
     * Renvoie si l'écriture est équilibrée (TotalDebit = TotalCrédit)
     *
     * @return boolean
     */
    public boolean isEquilibree() {
        boolean vRetour = this.getTotalDebit().compareTo(getTotalCredit()) == 0;
        return vRetour;
    }

    // ==================== Méthodes ====================

    @Override
    public int hashCode() {
        final int vPrime = 31;
        int vResult = 1;
        vResult = vPrime * vResult + ((id == null) ? 0 : id.hashCode());
        vResult = vPrime * vResult + ((journal == null) ? 0 : journal.hashCode());
        vResult = vPrime * vResult + ((reference == null) ? 0 : reference.hashCode());
        vResult = vPrime * vResult + ((date == null) ? 0 : date.hashCode());
        vResult = vPrime * vResult + ((libelle == null) ? 0 : libelle.hashCode());
        vResult = vPrime * vResult + (listLigneEcriture.hashCode());
        return vResult;
    }

    @Override
    public boolean equals(Object pObj) {
        if (this == pObj) {
            return true;
        }
        if (pObj == null) {
            return false;
        }
        if (getClass() != pObj.getClass()) {
            return false;
        }
        EcritureComptable vOther = (EcritureComptable) pObj;
        if (id == null) {
            if (vOther.id != null) {
                return false;
            }
        } else if (!id.equals(vOther.id)) {
            return false;
        }
        if (journal == null) {
            if (vOther.journal != null) {
                return false;
            }
        } else if (!journal.equals(vOther.journal)) {
            return false;
        }
        if (reference == null) {
            if (vOther.reference != null) {
                return false;
            }
        } else if (!reference.equals(vOther.reference)) {
            return false;
        }
        if (date == null) {
            if (vOther.date != null) {
                return false;
            }
        } else if (!date.equals(vOther.date)) {
            return false;
        }
        if (libelle == null) {
            if (vOther.libelle != null) {
                return false;
            }
        } else if (!libelle.equals(vOther.libelle)) {
            return false;
        }
        if (!listLigneEcriture.equals(vOther.listLigneEcriture)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append("id=").append(id)
                .append(vSEP).append("journal=").append(journal)
                .append(vSEP).append("reference='").append(reference).append('\'')
                .append(vSEP).append("date=").append(date)
                .append(vSEP).append("libelle='").append(libelle).append('\'')
                .append(vSEP).append("totalDebit=").append(this.getTotalDebit().toPlainString())
                .append(vSEP).append("totalCredit=").append(this.getTotalCredit().toPlainString())
                .append(vSEP).append("listLigneEcriture=[\n")
                .append(StringUtils.join(listLigneEcriture, "\n")).append("\n]")
                .append("}");
        return vStB.toString();
    }
}
