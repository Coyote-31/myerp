package com.dummy.myerp.model.bean.comptabilite;

import java.util.List;
import java.util.Objects;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

/**
 * Bean représentant un Compte Comptable
 */
public class CompteComptable {
    // ==================== Attributs ====================
    /** The Numero. */
    @NotNull
    private Integer numero;

    /** The Libelle. */
    @NotNull
    @Size(min = 1, max = 150)
    private String libelle;

    // ==================== Constructeurs ====================
    /**
     * Instantiates a new Compte comptable.
     */
    public CompteComptable() {
    }

    /**
     * Instantiates a new Compte comptable.
     *
     * @param pNumero the numero
     */
    public CompteComptable(Integer pNumero) {
        numero = pNumero;
    }

    /**
     * Instantiates a new Compte comptable.
     *
     * @param pNumero the numero
     * @param pLibelle the libelle
     */
    public CompteComptable(Integer pNumero, String pLibelle) {
        numero = pNumero;
        libelle = pLibelle;
    }

    // ==================== Getters/Setters ====================
    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer pNumero) {
        numero = pNumero;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }

    // ==================== Méthodes ====================

    @Override
    public int hashCode() {
        final int vPrime = 31;
        int vResult = 1;
        vResult = vPrime * vResult + ((numero == null) ? 0 : numero.hashCode());
        vResult = vPrime * vResult + ((libelle == null) ? 0 : libelle.hashCode());
        return vResult;
    }

    @Override
    public boolean equals(Object pObj) {
        if (this == pObj) {
            return true;
        }
        if (pObj == null) {
            return false;
        }
        if (getClass() != pObj.getClass()) {
            return false;
        }
        CompteComptable vOther = (CompteComptable) pObj;
        if (numero == null) {
            if (vOther.numero != null) {
                return false;
            }
        } else if (!numero.equals(vOther.numero)) {
            return false;
        }
        if (libelle == null) {
            if (vOther.libelle != null) {
                return false;
            }
        } else if (!libelle.equals(vOther.libelle)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append("numero=").append(numero)
                .append(vSEP).append("libelle='").append(libelle).append('\'')
                .append("}");
        return vStB.toString();
    }

    // ==================== Méthodes STATIC ====================
    /**
     * Renvoie le {@link CompteComptable} de numéro {@code pNumero} s'il est présent dans la liste
     *
     * @param pList la liste où chercher le {@link CompteComptable}
     * @param pNumero le numero du {@link CompteComptable} à chercher
     * @return {@link CompteComptable} ou {@code null}
     */
    public static CompteComptable getByNumero(List<? extends CompteComptable> pList, Integer pNumero) {
        CompteComptable vRetour = null;
        for (CompteComptable vBean : pList) {
            if (vBean != null && Objects.equals(vBean.getNumero(), pNumero)) {
                vRetour = vBean;
                break;
            }
        }
        return vRetour;
    }
}
