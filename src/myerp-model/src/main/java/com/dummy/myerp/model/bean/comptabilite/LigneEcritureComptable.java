package com.dummy.myerp.model.bean.comptabilite;

import java.math.BigDecimal;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import com.dummy.myerp.model.validation.constraint.MontantComptable;

/**
 * Bean représentant une Ligne d'écriture comptable.
 */
public class LigneEcritureComptable {

    // ==================== Attributs ====================
    /** Compte Comptable */
    @NotNull
    private CompteComptable compteComptable;

    /** The Libelle. */
    @Size(max = 200)
    private String libelle;

    /** The Debit. */
    @MontantComptable
    private BigDecimal debit;

    /** The Credit. */
    @MontantComptable
    private BigDecimal credit;

    // ==================== Constructeurs ====================
    /**
     * Instantiates a new Ligne ecriture comptable.
     */
    public LigneEcritureComptable() {
    }

    /**
     * Instantiates a new Ligne ecriture comptable.
     *
     * @param pCompteComptable the Compte Comptable
     * @param pLibelle the libelle
     * @param pDebit the debit
     * @param pCredit the credit
     */
    public LigneEcritureComptable(CompteComptable pCompteComptable, String pLibelle,
            BigDecimal pDebit, BigDecimal pCredit) {
        compteComptable = pCompteComptable;
        libelle = pLibelle;
        debit = pDebit;
        credit = pCredit;
    }

    // ==================== Getters/Setters ====================
    public CompteComptable getCompteComptable() {
        return compteComptable;
    }

    public void setCompteComptable(CompteComptable pCompteComptable) {
        compteComptable = pCompteComptable;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String pLibelle) {
        libelle = pLibelle;
    }

    public BigDecimal getDebit() {
        return debit;
    }

    public void setDebit(BigDecimal pDebit) {
        debit = pDebit;
    }

    public BigDecimal getCredit() {
        return credit;
    }

    public void setCredit(BigDecimal pCredit) {
        credit = pCredit;
    }

    // ==================== Méthodes ====================

    @Override
    public int hashCode() {
        final int vPrime = 31;
        int vResult = 1;
        vResult = vPrime * vResult + ((compteComptable == null) ? 0 : compteComptable.hashCode());
        vResult = vPrime * vResult + ((libelle == null) ? 0 : libelle.hashCode());
        vResult = vPrime * vResult + ((debit == null) ? 0 : debit.hashCode());
        vResult = vPrime * vResult + ((credit == null) ? 0 : credit.hashCode());
        return vResult;
    }

    @Override
    public boolean equals(Object pObj) {
        if (this == pObj) {
            return true;
        }
        if (pObj == null) {
            return false;
        }
        if (getClass() != pObj.getClass()) {
            return false;
        }
        LigneEcritureComptable vOther = (LigneEcritureComptable) pObj;
        if (compteComptable == null) {
            if (vOther.compteComptable != null) {
                return false;
            }
        } else if (!compteComptable.equals(vOther.compteComptable)) {
            return false;
        }
        if (libelle == null) {
            if (vOther.libelle != null) {
                return false;
            }
        } else if (!libelle.equals(vOther.libelle)) {
            return false;
        }
        if (debit == null) {
            if (vOther.debit != null) {
                return false;
            }
        } else if (!debit.equals(vOther.debit)) {
            return false;
        }
        if (credit == null) {
            if (vOther.credit != null) {
                return false;
            }
        } else if (!credit.equals(vOther.credit)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append(compteComptable)
                .append(vSEP).append("libelle='").append(libelle).append('\'')
                .append(vSEP).append("debit=").append(debit)
                .append(vSEP).append("credit=").append(credit)
                .append("}");
        return vStB.toString();
    }
}
