package com.dummy.myerp.model.validation.constraint;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import jakarta.validation.constraints.Digits;

/**
 * Contrainte à apposer sur les attibuts de type "montant comptable"
 *
 *  Cette contrainte est composée de :
 *  <ul>
 *      <li>@{@link Digits}</li>
 *  </ul>
 *
 *  Types supportés :
 *  <ul>
 *      <li>{@link java.math.BigDecimal}</li>
 *  </ul>
 */
@Digits(integer = 13, fraction = 2)
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {})
@Documented
public @interface MontantComptable {

    /** Message de la violation
     * @return {@link String} : Le message
    */
    String message() default "Taux de TVA invalide";

    /** Groupe de validation
     * @return {@link Class}[] : Class&lt;?&gt; array
    */
    Class<?>[] groups() default {};

    /** Payload
     * @return {@link Class}[] : Class&lt;? extend {@link Payload}&gt; array
    */
    Class<? extends Payload>[] payload() default {};

    /**
     * Interface permettant la déclaration de plusieurs {@link MontantComptable}
     */
    @Target({ ElementType.METHOD, ElementType.FIELD, ElementType.ANNOTATION_TYPE })
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List {
        /** List des {@link MontantComptable}
         * @return {@link MontantComptable}[] : Tableau de montant comptable
        */
        MontantComptable[] value();
    }
}
