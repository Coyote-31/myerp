package com.dummy.myerp.model.bean.comptabilite;

import jakarta.validation.constraints.NotNull;

/**
 * Bean représentant une séquence pour les références d'écriture comptable
 */
public class SequenceEcritureComptable {

    // ==================== Attributs ====================
    /** Journal comptable */
    @NotNull
    private JournalComptable journal;
    /** L'année */
    private Integer annee;
    /** La dernière valeur utilisée */
    private Integer derniereValeur;

    // ==================== Constructeurs ====================
    /**
     * Constructeur
     */
    public SequenceEcritureComptable() {
    }

    /**
     * Constructeur
     *
     * @param pJournal Le journal comptable
     * @param pAnnee L'année
     * @param pDerniereValeur La dernière valeur
     */
    public SequenceEcritureComptable(JournalComptable pJournal, Integer pAnnee, Integer pDerniereValeur) {
        journal = pJournal;
        annee = pAnnee;
        derniereValeur = pDerniereValeur;
    }

    // ==================== Getters/Setters ====================

    public JournalComptable getJournal() {
        return journal;
    }

    public void setJournal(JournalComptable pJournal) {
        this.journal = pJournal;
    }

    public Integer getAnnee() {
        return annee;
    }

    public void setAnnee(Integer pAnnee) {
        annee = pAnnee;
    }

    public Integer getDerniereValeur() {
        return derniereValeur;
    }

    public void setDerniereValeur(Integer pDerniereValeur) {
        derniereValeur = pDerniereValeur;
    }

    @Override
    public int hashCode() {
        final int vPrime = 31;
        int vResult = 1;
        vResult = vPrime * vResult + ((journal == null) ? 0 : journal.hashCode());
        vResult = vPrime * vResult + ((annee == null) ? 0 : annee.hashCode());
        vResult = vPrime * vResult + ((derniereValeur == null) ? 0 : derniereValeur.hashCode());
        return vResult;
    }

    @Override
    public boolean equals(Object pObj) {
        if (this == pObj) {
            return true;
        }
        if (pObj == null) {
            return false;
        }
        if (getClass() != pObj.getClass()) {
            return false;
        }
        SequenceEcritureComptable vOther = (SequenceEcritureComptable) pObj;
        if (journal == null) {
            if (vOther.journal != null) {
                return false;
            }
        } else if (!journal.equals(vOther.journal)) {
            return false;
        }
        if (annee == null) {
            if (vOther.annee != null) {
                return false;
            }
        } else if (!annee.equals(vOther.annee)) {
            return false;
        }
        if (derniereValeur == null) {
            if (vOther.derniereValeur != null) {
                return false;
            }
        } else if (!derniereValeur.equals(vOther.derniereValeur)) {
            return false;
        }
        return true;
    }

    // ==================== Méthodes ====================
    @Override
    public String toString() {
        final StringBuilder vStB = new StringBuilder(this.getClass().getSimpleName());
        final String vSEP = ", ";
        vStB.append("{")
                .append("journalCode=").append(journal.getCode())
                .append(vSEP)
                .append("annee=").append(annee)
                .append(vSEP)
                .append("derniereValeur=").append(derniereValeur)
                .append("}");
        return vStB.toString();
    }

}
