package com.dummy.myerp.testbusiness.business;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.transaction.TransactionStatus;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.impl.TransactionManager;

/**
 * Classe de test de l'initialisation du contexte Spring
 */
public class InitSpringIT extends BusinessTestCase {

    /**
     * Constructeur.
     */
    public InitSpringIT() {
        super();
    }

    /**
     * Teste l'initialisation du contexte Spring
     */
    @Test
    void testInit() {
        SpringRegistry.init();
        assertNotNull(SpringRegistry.getBusinessProxy());
        assertNotNull(SpringRegistry.getTransactionManager());
    }

    /**
     * Teste l'accès au ComptabilitéManager depuis le BusinessProxy
     */
    @Test
    void test_getComptabiliteManager_fromBusinessProxy() {
        SpringRegistry.init();
        BusinessProxy vBProxy = SpringRegistry.getBusinessProxy();
        assertNotNull(vBProxy.getComptabiliteManager());
    }

    /**
     * Teste le Transaction manager
     */
    @Test
    void test_TransactionManager() {
        SpringRegistry.init();
        TransactionManager vTManager = SpringRegistry.getTransactionManager();
        TransactionStatus vTS = vTManager.beginTransactionMyERP();
        assertThat(vTS.isNewTransaction()).isTrue();
        vTManager.commitMyERP(vTS);
        assertThat(vTS.isCompleted()).isTrue();
        vTS = vTManager.beginTransactionMyERP();
        assertThat(vTS.isNewTransaction()).isTrue();
        vTManager.rollbackMyERP(vTS);
        assertThat(vTS.isCompleted()).isTrue();
    }
}
