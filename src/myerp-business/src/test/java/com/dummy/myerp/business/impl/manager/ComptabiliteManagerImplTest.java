package com.dummy.myerp.business.impl.manager;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.dummy.myerp.business.contrat.BusinessProxy;
import com.dummy.myerp.business.impl.TransactionManager;
import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.consumer.dao.contrat.DaoProxy;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.exception.FunctionalException;
import com.dummy.myerp.technical.exception.NotFoundException;

@ExtendWith(MockitoExtension.class)
public class ComptabiliteManagerImplTest {

    private ComptabiliteManagerImpl manager = new ComptabiliteManagerImpl();

    @Mock
    private static DaoProxy daoProxy;
    @Mock
    private static ComptabiliteDao comptabiliteDao;
    @Mock
    private static BusinessProxy businessProxy;
    @Mock
    private static TransactionManager transactionManager;

    @Test
    public void addReference_ofNewSequence() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        // Sequence comptable
        SequenceEcritureComptable vSequenceEcritureComptable;
        vSequenceEcritureComptable = new SequenceEcritureComptable(
                new JournalComptable("AC", "Achat"),
                2023,
                1);

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getSequenceEcritureComptable(
                vEcritureComptable.getJournal().getCode(), 2023))
                .thenThrow(NotFoundException.class);

        manager.addReference(vEcritureComptable);

        verify(comptabiliteDao, times(1))
                .getSequenceEcritureComptable(vEcritureComptable.getJournal().getCode(), 2023);
        verify(comptabiliteDao, times(1)).insertSequenceEcritureComptable(vSequenceEcritureComptable);
    }

    @Test
    public void addReference_ofExistingSequence() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        // Sequence comptable - Get
        SequenceEcritureComptable vSequenceEcritureComptableGet;
        vSequenceEcritureComptableGet = new SequenceEcritureComptable(
                new JournalComptable("AC", "Achat"),
                2023,
                9);

        // Sequence comptable - ToUpdate
        SequenceEcritureComptable vSequenceEcritureComptableToUpdate;
        vSequenceEcritureComptableToUpdate = new SequenceEcritureComptable(
                new JournalComptable("AC", "Achat"),
                2023,
                10);

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getSequenceEcritureComptable(
                vEcritureComptable.getJournal().getCode(), 2023))
                .thenReturn(vSequenceEcritureComptableGet);

        manager.addReference(vEcritureComptable);

        verify(comptabiliteDao, times(1))
                .getSequenceEcritureComptable(vEcritureComptable.getJournal().getCode(), 2023);
        verify(comptabiliteDao, times(1)).updateSequenceEcritureComptable(vSequenceEcritureComptableToUpdate);
    }

    @Test
    public void checkEcritureComptableUnit() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));
        manager.checkEcritureComptableUnit(vEcritureComptable);
    }

    @Test
    public void checkEcritureComptableUnitViolation() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();

        assertThatThrownBy(() -> manager.checkEcritureComptableUnit(vEcritureComptable))
                .isInstanceOf(FunctionalException.class)
                .hasMessage("L'écriture comptable ne respecte pas les règles de gestion.");
    }

    @Test
    public void checkEcritureComptableUnitRG2() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setDate(new Date());
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(1234)));

        assertThatThrownBy(() -> manager.checkEcritureComptableUnit(vEcritureComptable))
                .isInstanceOf(FunctionalException.class)
                .hasMessage("L'écriture comptable n'est pas équilibrée.");
    }

    @Test
    public void checkEcritureComptableUnitRG3() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setDate(new Date());
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(
                        new CompteComptable(1),
                        null,
                        new BigDecimal(123),
                        null));
        vEcritureComptable.getListLigneEcriture().add(
                new LigneEcritureComptable(
                        new CompteComptable(1),
                        null,
                        new BigDecimal(123),
                        null));

        assertThatThrownBy(() -> manager.checkEcritureComptableUnit(vEcritureComptable))
                .isInstanceOf(FunctionalException.class)
                .hasMessage(
                        "L'écriture comptable doit avoir au moins deux lignes : une ligne au débit et une ligne au crédit.");
    }

    @Test
    public void checkEcritureComptableUnitRG5_CodeJournal() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setReference("CB-2023/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        assertThatThrownBy(() -> manager.checkEcritureComptableUnit(vEcritureComptable))
                .isInstanceOf(FunctionalException.class)
                .hasMessage(
                        "Le code journal dans référence de l'écriture comptable doit correspondre au code du journal de l'écriture comptable.");
    }

    @Test
    public void checkEcritureComptableUnitRG5_Annee() throws Exception {
        EcritureComptable vEcritureComptable;
        vEcritureComptable = new EcritureComptable();
        vEcritureComptable.setJournal(new JournalComptable("AC", "Achat"));
        vEcritureComptable.setReference("AC-2020/00001");
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEcritureComptable.setDate(dateFormatter.parse("1-Janvier-2023"));
        vEcritureComptable.setLibelle("Libelle");
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEcritureComptable.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        assertThatThrownBy(() -> manager.checkEcritureComptableUnit(vEcritureComptable))
                .isInstanceOf(FunctionalException.class)
                .hasMessage(
                        "L'année dans la référence de l'écriture comptable doit correspondre à la date de l'écriture comptable.");
    }

    @Test
    public void checkEcritureComptableContext_ofNewEcriture() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("1-Janvier-2023"));
        vEC_Given.setLibelle("Libelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenThrow(NotFoundException.class);

        manager.checkEcritureComptableContext(vEC_Given);

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
    }

    @Test
    public void checkEcritureComptableContext_ofNewEcriture_returnFunctionalException() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("1-Janvier-2023"));
        vEC_Given.setLibelle("Libelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        // Ecriture comptable retournée pas la DAO
        EcritureComptable vEC_DAO;
        vEC_DAO = new EcritureComptable();
        vEC_DAO.setId(1);
        vEC_DAO.setJournal(new JournalComptable("AC", "Achat"));
        vEC_DAO.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterDAO = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_DAO.setDate(dateFormatterDAO.parse("1-Janvier-2023"));
        vEC_DAO.setLibelle("Libelle");
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenReturn(vEC_DAO);

        assertThatThrownBy(() -> manager.checkEcritureComptableContext(vEC_Given))
                .isInstanceOf(FunctionalException.class)
                .hasMessage(
                        "Une autre écriture comptable existe déjà avec la même référence.");

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
    }

    @Test
    public void checkEcritureComptableContext_ofExistingEcriture() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setId(1);
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("2-Janvier-2023"));
        vEC_Given.setLibelle("NewLibelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(345), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(345)));

        // Ecriture comptable retournée pas la DAO
        EcritureComptable vEC_DAO;
        vEC_DAO = new EcritureComptable();
        vEC_DAO.setId(1);
        vEC_DAO.setJournal(new JournalComptable("AC", "Achat"));
        vEC_DAO.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterDAO = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_DAO.setDate(dateFormatterDAO.parse("1-Janvier-2023"));
        vEC_DAO.setLibelle("Libelle");
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenReturn(vEC_DAO);

        manager.checkEcritureComptableContext(vEC_Given);

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
    }

    @Test
    public void checkEcritureComptableContext_ofExistingEcriture_returnFunctionalException() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setId(2);
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("1-Janvier-2023"));
        vEC_Given.setLibelle("Libelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        // Ecriture comptable retournée pas la DAO
        EcritureComptable vEC_DAO;
        vEC_DAO = new EcritureComptable();
        vEC_DAO.setId(1);
        vEC_DAO.setJournal(new JournalComptable("AC", "Achat"));
        vEC_DAO.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterDAO = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_DAO.setDate(dateFormatterDAO.parse("1-Janvier-2023"));
        vEC_DAO.setLibelle("Libelle");
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenReturn(vEC_DAO);

        assertThatThrownBy(() -> manager.checkEcritureComptableContext(vEC_Given))
                .isInstanceOf(FunctionalException.class)
                .hasMessage(
                        "Une autre écriture comptable existe déjà avec la même référence.");

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
    }

    @Test
    public void checkEcritureComptable() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("1-Janvier-2023"));
        vEC_Given.setLibelle("Libelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenThrow(NotFoundException.class);

        manager.checkEcritureComptable(vEC_Given);

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
    }

    @Test
    public void insertEcritureComptable() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("1-Janvier-2023"));
        vEC_Given.setLibelle("Libelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenThrow(NotFoundException.class);

        manager.insertEcritureComptable(vEC_Given);

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
        verify(comptabiliteDao, times(1)).insertEcritureComptable(vEC_Given);
        verify(transactionManager, times(1)).beginTransactionMyERP();
        verify(transactionManager, times(1)).commitMyERP(null);
        verify(transactionManager, times(1)).rollbackMyERP(null);

    }

    @Test
    public void updateEcritureComptable() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable de test
        EcritureComptable vEC_Given;
        vEC_Given = new EcritureComptable();
        vEC_Given.setId(1);
        vEC_Given.setJournal(new JournalComptable("AC", "Achat"));
        vEC_Given.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterGiven = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_Given.setDate(dateFormatterGiven.parse("2-Janvier-2023"));
        vEC_Given.setLibelle("NewLibelle");
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(345), null));
        vEC_Given.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(345)));

        // Ecriture comptable retournée pas la DAO
        EcritureComptable vEC_DAO;
        vEC_DAO = new EcritureComptable();
        vEC_DAO.setId(1);
        vEC_DAO.setJournal(new JournalComptable("AC", "Achat"));
        vEC_DAO.setReference("AC-2023/00001");
        SimpleDateFormat dateFormatterDAO = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
        vEC_DAO.setDate(dateFormatterDAO.parse("1-Janvier-2023"));
        vEC_DAO.setLibelle("Libelle");
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(1), null, new BigDecimal(123), null));
        vEC_DAO.getListLigneEcriture().add(new LigneEcritureComptable(
                new CompteComptable(2), null, null, new BigDecimal(123)));

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        when(comptabiliteDao.getEcritureComptableByRef(vEC_Given.getReference()))
                .thenReturn(vEC_DAO);

        manager.updateEcritureComptable(vEC_Given);

        verify(comptabiliteDao, times(1)).getEcritureComptableByRef(vEC_Given.getReference());
        verify(comptabiliteDao, times(1)).updateEcritureComptable(vEC_Given);
        verify(transactionManager, times(1)).beginTransactionMyERP();
        verify(transactionManager, times(1)).commitMyERP(null);
        verify(transactionManager, times(1)).rollbackMyERP(null);

    }

    @Test
    public void deleteEcritureComptable() throws Exception {

        ComptabiliteManagerImpl.configure(businessProxy, daoProxy, transactionManager);

        // Ecriture comptable id
        int vECid = 1;

        when(daoProxy.getComptabiliteDao()).thenReturn(comptabiliteDao);

        manager.deleteEcritureComptable(vECid);

        verify(comptabiliteDao, times(1)).deleteEcritureComptable(vECid);
        verify(transactionManager, times(1)).beginTransactionMyERP();
        verify(transactionManager, times(1)).commitMyERP(null);
        verify(transactionManager, times(1)).rollbackMyERP(null);

    }

}
