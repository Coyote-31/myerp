package com.dummy.myerp.consumer.db.helper;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang3.time.DateUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class ResultSetHelperTest {

    String vColName;

    @Mock
    ResultSet vRS;

    @BeforeEach
    void init() {
        vColName = "column_name";
    }

    @Test
    void getInteger_returnInteger() throws SQLException {
        when(vRS.getInt(vColName)).thenReturn(1);
        Integer vResult = ResultSetHelper.getInteger(vRS, vColName);
        assertThat(vResult).isEqualTo(1);
    }

    @Test
    void getInteger_returnNull() throws SQLException {
        when(vRS.getInt(vColName)).thenReturn(0);
        when(vRS.wasNull()).thenReturn(true);
        Integer vResult = ResultSetHelper.getInteger(vRS, vColName);
        assertThat(vResult).isEqualTo(null);
    }

    @Test
    void getLong_returnLong() throws SQLException {
        when(vRS.getLong(vColName)).thenReturn(1L);
        Long vResult = ResultSetHelper.getLong(vRS, vColName);
        assertThat(vResult).isEqualTo(1L);
    }

    @Test
    void getLong_returnNull() throws SQLException {
        when(vRS.getLong(vColName)).thenReturn(0L);
        when(vRS.wasNull()).thenReturn(true);
        Long vResult = ResultSetHelper.getLong(vRS, vColName);
        assertThat(vResult).isEqualTo(null);
    }

    @Test
    void getDate_returnDateTruncate() throws SQLException {
        Date vDate = new java.util.Date();
        when(vRS.getDate(vColName))
                .thenReturn(new java.sql.Date(vDate.getTime()));
        Date vDateAssert = DateUtils.truncate(vDate, Calendar.DATE);
        Date vResult = ResultSetHelper.getDate(vRS, vColName);
        assertThat(vResult).isEqualTo(vDateAssert);
    }

    @Test
    void getDate_returnNull() throws SQLException {
        when(vRS.getDate(vColName)).thenReturn(null);
        Date vResult = ResultSetHelper.getDate(vRS, vColName);
        assertThat(vResult).isNull();
    }

}
