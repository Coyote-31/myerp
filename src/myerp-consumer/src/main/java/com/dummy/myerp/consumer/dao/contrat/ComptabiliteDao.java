package com.dummy.myerp.consumer.dao.contrat;

import java.util.List;

import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.exception.NotFoundException;

/**
 * Interface de DAO des objets du package Comptabilite
 */
public interface ComptabiliteDao {

    /**
     * Renvoie la liste des Comptes Comptables
     *
     * @return {@link List}
     */
    List<CompteComptable> getListCompteComptable();

    /**
     * Renvoie la liste des Journaux Comptables
     *
     * @return {@link List}
     */
    List<JournalComptable> getListJournalComptable();

    // ==================== SequenceEcritureComptable ====================

    /**
     * Renvoie la liste des Séquences des Ecritures Comptables
     *
     * @return {@link List}
     */
    List<SequenceEcritureComptable> getListSequenceEcritureComptable();

    /**
     * Renvoie la Séquence des Ecritures Comptables
     * de code de Journal Comptable {@code pJournalCode}
     * et d'année de Séquence d'Ecritures Comptables {@code pAnnee}
     *
     * @param pJournalCode le code du Journal Comptable
     * @param pAnnee       l'année de Séquence d'Ecritures Comptables
     * @return {@link SequenceEcritureComptable}
     * @throws NotFoundException Si la séquence des écritures comptables n'est pas trouvée
     */
    SequenceEcritureComptable getSequenceEcritureComptable(String pJournalCode, Integer pAnnee)
            throws NotFoundException;

    /**
     * Insert une nouvelle Séquence des Ecritures Comptables.
     *
     * @param pSequenceEcritureComptable - {@link SequenceEcritureComptable}
     */
    void insertSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable);

    /**
     * Met à jour la Séquence des Ecritures Comptables.
     *
     * @param pSequenceEcritureComptable - {@link SequenceEcritureComptable}
     */
    void updateSequenceEcritureComptable(SequenceEcritureComptable pSequenceEcritureComptable);

    /**
     * Supprime la Séquence des Ecritures Comptables
     * de code de Journal Comptable {@code pJournalCode}
     * et d'année de Séquence d'Ecritures Comptables {@code pAnnee}
     *
     * @param pJournalCode le code du Journal Comptable
     * @param pAnnee       l'année de Séquence d'Ecritures Comptables
     */
    void deleteSequenceEcritureComptable(String pJournalCode, Integer pAnnee);

    // ==================== EcritureComptable ====================

    /**
     * Renvoie la liste des Écritures Comptables
     *
     * @return {@link List}
     */
    List<EcritureComptable> getListEcritureComptable();

    /**
     * Renvoie l'Écriture Comptable d'id {@code pId}.
     *
     * @param pId l'id de l'écriture comptable
     * @return {@link EcritureComptable}
     * @throws NotFoundException Si l'écriture comptable n'est pas trouvée
     */
    EcritureComptable getEcritureComptable(Integer pId) throws NotFoundException;

    /**
     * Renvoie l'Écriture Comptable de référence {@code pRef}.
     *
     * @param pReference la référence de l'écriture comptable
     * @return {@link EcritureComptable}
     * @throws NotFoundException : Si l'écriture comptable n'est pas trouvée
     */
    EcritureComptable getEcritureComptableByRef(String pReference) throws NotFoundException;

    /**
     * Charge la liste des lignes d'écriture de l'écriture comptable
     * {@code pEcritureComptable}
     *
     * @param pEcritureComptable -
     */
    void loadListLigneEcriture(EcritureComptable pEcritureComptable);

    /**
     * Insert une nouvelle écriture comptable.
     *
     * @param pEcritureComptable -
     */
    void insertEcritureComptable(EcritureComptable pEcritureComptable);

    /**
     * Met à jour l'écriture comptable.
     *
     * @param pEcritureComptable -
     */
    void updateEcritureComptable(EcritureComptable pEcritureComptable);

    /**
     * Supprime l'écriture comptable d'id {@code pId}.
     *
     * @param pId l'id de l'écriture
     */
    void deleteEcritureComptable(Integer pId);
}
