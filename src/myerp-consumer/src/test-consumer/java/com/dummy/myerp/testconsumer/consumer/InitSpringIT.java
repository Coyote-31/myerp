package com.dummy.myerp.testconsumer.consumer;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

/**
 * Classe de test de l'initialisation du contexte Spring
 */
public class InitSpringIT extends ConsumerTestCase {

    /**
     * Constructeur.
     */
    public InitSpringIT() {
        super();
    }

    /**
     * Teste l'initialisation du contexte Spring
     */
    @Test
    void testInit() {
        SpringRegistry.init();
        assertNotNull(SpringRegistry.getDaoProxy());
    }
}
