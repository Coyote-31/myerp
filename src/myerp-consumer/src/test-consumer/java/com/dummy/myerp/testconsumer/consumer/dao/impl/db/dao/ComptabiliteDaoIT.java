package com.dummy.myerp.testconsumer.consumer.dao.impl.db.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import com.dummy.myerp.consumer.dao.contrat.ComptabiliteDao;
import com.dummy.myerp.model.bean.comptabilite.CompteComptable;
import com.dummy.myerp.model.bean.comptabilite.EcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.JournalComptable;
import com.dummy.myerp.model.bean.comptabilite.LigneEcritureComptable;
import com.dummy.myerp.model.bean.comptabilite.SequenceEcritureComptable;
import com.dummy.myerp.technical.exception.NotFoundException;
import com.dummy.myerp.testconsumer.consumer.ConsumerTestCase;

@ExtendWith(SpringExtension.class)
@ContextConfiguration("classpath:/com/dummy/myerp/testconsumer/consumer/bootstrapContext.xml")
@Transactional
public class ComptabiliteDaoIT extends ConsumerTestCase {

        private static ComptabiliteDao vComptabiliteDao;

        @BeforeAll
        static void initAll() {
                vComptabiliteDao = getDaoProxy().getComptabiliteDao();
        }

        // ==================== CompteComptable - GET ====================
        @Test
        void getListCompteComptable() {
                CompteComptable vCompteComptableAssert = new CompteComptable(706, "Prestations de services");

                List<CompteComptable> vList = vComptabiliteDao.getListCompteComptable();

                assertEquals(7, vList.size());
                assertThat(vList).containsOnlyOnce(vCompteComptableAssert);
        }

        // ==================== JournalComptable - GET ====================
        @Test
        void getListJournalComptable() {
                JournalComptable vJournalComptableAssert = new JournalComptable("OD", "Opérations Diverses");

                List<JournalComptable> vList = vComptabiliteDao.getListJournalComptable();

                assertEquals(4, vList.size());
                assertThat(vList).containsOnlyOnce(vJournalComptableAssert);
        }

        // ==================== SequenceEcritureComptable - GET ====================
        @Test
        void getListSequenceEcritureComptable() {
                SequenceEcritureComptable vSequenceEcritureComptableAssert = new SequenceEcritureComptable(
                                new JournalComptable("OD", "Opérations Diverses"),
                                2016,
                                88);

                List<SequenceEcritureComptable> vList = vComptabiliteDao.getListSequenceEcritureComptable();

                assertEquals(4, vList.size());
                assertThat(vList).containsOnlyOnce(vSequenceEcritureComptableAssert);
        }

        @Test
        void getSequenceEcritureComptable() throws NotFoundException {
                SequenceEcritureComptable vSequenceEcritureComptableAssert = new SequenceEcritureComptable(
                                new JournalComptable("OD", "Opérations Diverses"),
                                2016,
                                88);

                SequenceEcritureComptable vSequenceEcritureComptableReturn = vComptabiliteDao
                                .getSequenceEcritureComptable("OD", 2016);

                assertThat(vSequenceEcritureComptableReturn).isEqualTo(vSequenceEcritureComptableAssert);
        }

        @Test
        void getSequenceEcritureComptable_throwsNotFoundException() throws NotFoundException {

                assertThatThrownBy(() -> vComptabiliteDao.getSequenceEcritureComptable("DO", 2017))
                                .isInstanceOf(NotFoundException.class)
                                .hasMessage(
                                                "SequenceEcritureComptable non trouvée : journal_code=DO annee=2017");
        }

        // ==================== SequenceEcritureComptable - INSERT ====================
        @Test
        void insertSequenceEcritureComptable() {
                SequenceEcritureComptable vSequenceEcritureComptableInsert = new SequenceEcritureComptable(
                                new JournalComptable("OD", "Opérations Diverses"),
                                2017,
                                42);

                vComptabiliteDao.insertSequenceEcritureComptable(vSequenceEcritureComptableInsert);

                assertThat(vComptabiliteDao.getListSequenceEcritureComptable())
                                .hasSize(5)
                                .containsOnlyOnce(vSequenceEcritureComptableInsert);
        }

        // ==================== SequenceEcritureComptable - UPDATE ====================
        @Test
        void updateSequenceEcritureComptable() {
                SequenceEcritureComptable vSequenceEcritureComptableUpdate = new SequenceEcritureComptable(
                                new JournalComptable("VE", "Vente"),
                                2016,
                                42);

                vComptabiliteDao.updateSequenceEcritureComptable(vSequenceEcritureComptableUpdate);

                assertThat(vComptabiliteDao.getListSequenceEcritureComptable())
                                .hasSize(4)
                                .containsOnlyOnce(vSequenceEcritureComptableUpdate);
        }

        // ==================== SequenceEcritureComptable - DELETE ====================
        @Test
        void deleteSequenceEcritureComptable() {
                SequenceEcritureComptable vSequenceEcritureComptableDelete = new SequenceEcritureComptable(
                                new JournalComptable("OD", "Opérations Diverses"),
                                2017,
                                69);

                vComptabiliteDao.deleteSequenceEcritureComptable("OD", 2017);

                assertThat(vComptabiliteDao.getListSequenceEcritureComptable())
                                .hasSize(4)
                                .doesNotContain(vSequenceEcritureComptableDelete);

        }

        // ==================== EcritureComptable - GET ====================
        @Test
        void getListEcritureComptable() throws Exception {

                EcritureComptable vEcritureComptableAssert = new EcritureComptable();
                vEcritureComptableAssert.setId(-3);
                vEcritureComptableAssert.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptableAssert.setReference("BQ-2016/00003");

                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptableAssert.setDate(vDate);

                vEcritureComptableAssert.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptableAssert1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("52.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptableAssert2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("52.74"));

                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert1);
                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert2);

                List<EcritureComptable> vEcritureComptables = vComptabiliteDao.getListEcritureComptable();

                assertThat(vEcritureComptables)
                                .hasSize(5)
                                .containsOnlyOnce(vEcritureComptableAssert);
        }

        @Test
        void getEcritureComptable() throws Exception {

                EcritureComptable vEcritureComptableAssert = new EcritureComptable();
                vEcritureComptableAssert.setId(-3);
                vEcritureComptableAssert.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptableAssert.setReference("BQ-2016/00003");

                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptableAssert.setDate(vDate);

                vEcritureComptableAssert.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptableAssert1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("52.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptableAssert2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("52.74"));

                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert1);
                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert2);

                EcritureComptable vEcritureComptable = vComptabiliteDao.getEcritureComptable(-3);

                assertThat(vEcritureComptable).isEqualTo(vEcritureComptableAssert);
        }

        @Test
        void getEcritureComptableByRef() throws Exception {

                EcritureComptable vEcritureComptableAssert = new EcritureComptable();
                vEcritureComptableAssert.setId(-3);
                vEcritureComptableAssert.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptableAssert.setReference("BQ-2016/00003");

                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptableAssert.setDate(vDate);

                vEcritureComptableAssert.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptableAssert1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("52.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptableAssert2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("52.74"));

                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert1);
                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert2);

                EcritureComptable vEcritureComptable = vComptabiliteDao.getEcritureComptableByRef("BQ-2016/00003");

                assertThat(vEcritureComptable).isEqualTo(vEcritureComptableAssert);
        }

        @Test
        void getEcritureComptableByRef_throwsNotFoundException() throws NotFoundException {

                assertThatThrownBy(() -> vComptabiliteDao.getEcritureComptableByRef("BQ-2016/00007"))
                                .isInstanceOf(NotFoundException.class)
                                .hasMessage("EcritureComptable non trouvée : reference=BQ-2016/00007");
        }

        @Test
        void loadListLigneEcriture() throws Exception {

                EcritureComptable vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setId(-3);
                vEcritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptable.setReference("BQ-2016/00003");
                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptable.setDate(vDate);
                vEcritureComptable.setLibelle("Paiement Facture F110001");

                EcritureComptable vEcritureComptableAssert = new EcritureComptable();
                vEcritureComptableAssert.setId(-3);
                vEcritureComptableAssert.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptableAssert.setReference("BQ-2016/00003");

                SimpleDateFormat vDateFormatterAssert = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDateAssert = new Date(vDateFormatterAssert.parse("29-Décembre-2016").getTime());
                vEcritureComptableAssert.setDate(vDateAssert);

                vEcritureComptableAssert.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptableAssert1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("52.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptableAssert2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("52.74"));

                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert1);
                vEcritureComptableAssert.getListLigneEcriture().add(vLigneEcritureComptableAssert2);

                vComptabiliteDao.loadListLigneEcriture(vEcritureComptable);

                assertThat(vEcritureComptable).isEqualTo(vEcritureComptableAssert);
        }

        // ==================== EcritureComptable - INSERT ====================
        @Test
        void insertEcritureComptable() throws Exception {

                EcritureComptable vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptable.setReference("BQ-2016/00004");
                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("30-Décembre-2016").getTime());
                vEcritureComptable.setDate(vDate);
                vEcritureComptable.setLibelle("Paiement Facture F110002");

                LigneEcritureComptable vLigneEcritureComptable1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("60.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptable2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("60.74"));

                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable1);
                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable2);

                vComptabiliteDao.insertEcritureComptable(vEcritureComptable);

                assertThat(vComptabiliteDao.getListEcritureComptable())
                                .hasSize(6).containsOnlyOnce(vEcritureComptable);
        }

        // ==================== EcritureComptable - UPDATE ====================
        @Test
        void updateEcritureComptable() throws Exception {

                EcritureComptable vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setId(-3);
                vEcritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptable.setReference("BQ-2016/00003");
                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptable.setDate(vDate);
                vEcritureComptable.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptable1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("60.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptable2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("60.74"));

                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable1);
                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable2);

                vComptabiliteDao.updateEcritureComptable(vEcritureComptable);

                assertThat(vComptabiliteDao.getListEcritureComptable())
                                .hasSize(5).containsOnlyOnce(vEcritureComptable);
        }

        // ==================== EcritureComptable - DELETE ====================
        @Test
        void deleteEcritureComptable() throws Exception {

                EcritureComptable vEcritureComptable = new EcritureComptable();
                vEcritureComptable.setId(-3);
                vEcritureComptable.setJournal(new JournalComptable("BQ", "Banque"));
                vEcritureComptable.setReference("BQ-2016/00003");

                SimpleDateFormat vDateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.FRENCH);
                Date vDate = new Date(vDateFormatter.parse("29-Décembre-2016").getTime());
                vEcritureComptable.setDate(vDate);

                vEcritureComptable.setLibelle("Paiement Facture F110001");

                LigneEcritureComptable vLigneEcritureComptable1 = new LigneEcritureComptable(
                                new CompteComptable(401, "Fournisseurs"),
                                null,
                                new BigDecimal("52.74"),
                                null);

                LigneEcritureComptable vLigneEcritureComptable2 = new LigneEcritureComptable(
                                new CompteComptable(512, "Banque"),
                                null,
                                null,
                                new BigDecimal("52.74"));

                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable1);
                vEcritureComptable.getListLigneEcriture().add(vLigneEcritureComptable2);

                vComptabiliteDao.deleteEcritureComptable(-3);

                assertThat(vComptabiliteDao.getListEcritureComptable())
                                .hasSize(4).doesNotContain(vEcritureComptable);
        }
}
