package com.dummy.myerp.technical.exception;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

/**
 * Classe des Exception de type "Donnée non trouvée"
 */
public class NotFoundExceptionTest {

    @Test
    void constructorWithMsg() {
        String vAssertMsg = "Testing message";
        NotFoundException vFE_underTest = new NotFoundException(vAssertMsg);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithCause() {
        String vAssertMsg = "Testing message";
        Throwable vAssertCause = new Throwable(vAssertMsg);
        NotFoundException vFE_underTest = new NotFoundException(vAssertCause);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithMsgAndCause() {
        String vAssertMsg = "Testing message";
        String vCauseMsg = "Cause message";
        Throwable vAssertCause = new Throwable(vCauseMsg);
        NotFoundException vFE_underTest = new NotFoundException(vAssertMsg, vAssertCause);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vCauseMsg);
    }
}
