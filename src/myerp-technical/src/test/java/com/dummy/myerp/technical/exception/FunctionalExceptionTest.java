package com.dummy.myerp.technical.exception;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class FunctionalExceptionTest {

    @Test
    void constructorWithMsg() {
        String vAssertMsg = "Testing message";
        FunctionalException vFE_underTest = new FunctionalException(vAssertMsg);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithCause() {
        String vAssertMsg = "Testing message";
        Throwable vAssertCause = new Throwable(vAssertMsg);
        FunctionalException vFE_underTest = new FunctionalException(vAssertCause);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithMsgAndCause() {
        String vAssertMsg = "Testing message";
        String vCauseMsg = "Cause message";
        Throwable vAssertCause = new Throwable(vCauseMsg);
        FunctionalException vFE_underTest = new FunctionalException(vAssertMsg, vAssertCause);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vCauseMsg);
    }
}
