package com.dummy.myerp.technical.exception;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class TechnicalExceptionTest {

    @Test
    void constructorWithMsg() {
        String vAssertMsg = "Testing message";
        TechnicalException vFE_underTest = new TechnicalException(vAssertMsg);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithCause() {
        String vAssertMsg = "Testing message";
        Throwable vAssertCause = new Throwable(vAssertMsg);
        TechnicalException vFE_underTest = new TechnicalException(vAssertCause);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vAssertMsg);
    }

    @Test
    void constructorWithMsgAndCause() {
        String vAssertMsg = "Testing message";
        String vCauseMsg = "Cause message";
        Throwable vAssertCause = new Throwable(vCauseMsg);
        TechnicalException vFE_underTest = new TechnicalException(vAssertMsg, vAssertCause);
        assertThat(vFE_underTest.getMessage()).isEqualTo(vAssertMsg);
        assertThat(vFE_underTest.getCause()).isEqualTo(vAssertCause);
        assertThat(vFE_underTest.getCause().getMessage()).isEqualTo(vCauseMsg);
    }
}
